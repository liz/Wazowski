# Wazowski

Meeting transcription and summarization, all running in local models without sending any data to the cloud.

# Requirements

- Python 3.10 with virtualenv
- ffmpeg

# Installation

```bash
virtualenv Wazowski --python=python3.9.9
source Wazowski/bin/activate
pip install -r requirements.txt
```

# Usage

```bash
python3 wazowski.py -i {meeting_video_recording} -o {meeting_summary_output}
```

# Build

```bash
pyinstaller --onefile -n Wazowski wazowski.py
```
